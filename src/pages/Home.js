import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Container } from 'react-bootstrap';
 
 
const Home = () => {
   const pageData = {
       title: "The Zuitt Shop",
       content: "Products for everyone, everywhere",
       destination: "/products",
       label: "Browse Products"
   };
 
   return(
       <React.Fragment>
           <br/>
           <Banner data={pageData}/>
           <Container fluid>
                <br/>
               <h2 className="text-center mb-4">Featured Products</h2>
               <Highlights/>
           </Container>
       </React.Fragment>
   );
}
export default Home;
