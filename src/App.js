import './App.css';
import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'
import Products from './pages/Products';
import Specific from './pages/Specific';
import MyCart from './pages/MyCart';
import Orders from './pages/Orders';
import Error from './pages/Error';
//import '../node_modules/bootstrap/dist/css/bootstrap.css';
//import 'bootstrap/dist/css/bootstrap.css';
import 'bootswatch/dist/cosmo/bootstrap.min.css';
import { UserProvider } from './UserContext';
import Footer from './components/Footer';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  useEffect(()=> {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      // fetch('http://localhost:3000/users/details', {
      headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({id: data._id, isAdmin: data.isAdmin})
      } else {
        setUser({id: null, isAdmin: null})
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser}}>
       <Router>
           <NavBar />
           <Routes>
               <Route path ='/' element={ <Home />} />
               <Route path ='/register' element={ <Register />} />
               <Route path ='/login' element={ <Login />} />
               <Route path ='/products' element={ <Products />} />
               <Route path ='/products/:productId' element={ <Specific />} />
               <Route path ='/cart' element={ <MyCart />} />
               <Route path ='/orders' element={ <Orders />} />
               <Route path ='/logout' element={ <Logout />} />
               <Route element={ <Error />}/>
           </Routes>
           <Footer />
       </Router>
   </UserProvider>
  );
}

export default App;