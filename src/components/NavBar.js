import React, { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
 
const NavBar = () => {
 
   const { user } = useContext(UserContext)

   return(


    <Navbar bg="dark" variant="dark" expand="lg">
        <Container>
        <Link className="navbar-brand" to="/">The Zuitt Shop</Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <Link className="nav-link" to="/products">
                    {user.isAdmin === true ?
                            <span>Admin Dashboard</span>
                        :
                            <span>Products</span>
                    }  
                </Link>
            </Nav>
            <Nav className="mr-auto">
            {/* <Nav className="ml-auto"> */}
                {user.id !== null ?
                        user.isAdmin === true ?
                                <Link className="nav-link" to="/logout">
                                    Log Out
                                </Link>
                            :
                                <React.Fragment>
                                    <Link className="nav-link" to="/cart">
                                        Cart
                                    </Link>
                                    <Link className="nav-link" to="/orders">
                                        Orders
                                    </Link>
                                    <Link className="nav-link" to="/logout">
                                        Log Out
                                    </Link>
                                </React.Fragment>
                    :
                        <React.Fragment>
                            <Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}}>
                                Log In
                            </Link>
                            <Link className="nav-link" to="/register">
                                Register
                            </Link>
                        </React.Fragment>
                }              
            </Nav>
        </Navbar.Collapse>
        </Container>
    </Navbar>
    );
}

/*
<Navbar bg="light" variant="light" sticky="top" className="navbar">
<Container>
<Navbar.Brand href="/">Zuitt Shop</Navbar.Brand>
<Navbar.Collapse id="responsive-navbar-nav">
<Nav className="me-auto">
<Link className="nav-link" to="/products">
{ user.isAdmin === true ? <span>Admin Dashboard</span> : <span>Products</span> }
</Link>
</Nav>
<Nav className="mr-auto">
{user.id !== null
?
user.isAdmin === true ? <Link className="nav-link" to="/logout">Log Out</Link> :
<React.Fragment>
<Link className="nav-link" to="/cart">Cart</Link>
<Link className="nav-link" to="/orders">Orders</Link>
<Link className="nav-link" to="/logout">Log Out</Link>
</React.Fragment>
:
<React.Fragment>
<Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}}>Log In</Link>
<Link className="nav-link" to="/register">Register</Link>
</React.Fragment>
}
</Nav>
</Navbar.Collapse>
</Container>
</Navbar>

   )};
*/

export default NavBar;