import React from 'react';
import {   Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
 
 
const Banner = ({ data }) => {
   const { title, content, destination, label } = data;
   return(
       <Row>
           <Col>
               <div className="text-center">
                   <h1>{title}</h1>
                   <p id="motto">{content}</p>
                   <Link className="btn btn-primary" to={destination}>
                       {label}
                   </Link>
               </div>
           </Col>
       </Row>
   );
}
export default Banner;