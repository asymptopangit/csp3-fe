
import React from "react";

const Footer = () => {
    return(
        <div className="main-footer">
            <div className="container">
                <div className="row">
                    <p className="col-sm">Copyright© Steve De Lazo</p>
                </div>
            </div>
        </div>
    )
}

export default Footer;